#include <iostream>
#include <vector>
#include "../Headers/Face.h"
#include "../Headers/Aresta.h"
#include "../Headers/Ponto.h"
#include "../Headers/image.hpp"

using namespace std;

Face::Face(){}
Face::~Face(){}

void Face::calcpZ(){

    float zmedio = 0;
    for(int i = 0; i < arestas.size() ; i++){
        zmedio += arestas[i].getP1().getZ(); 
        zmedio += arestas[i].getP2().getZ();
    }
    pZ = zmedio/(arestas.size()*2);
      
}

float Face::getpZ(){
    return pZ;
}
        
void Face::printFace(image *im, float minZ, float d){
    for(int i = 0; i < arestas.size() ; i++){
        arestas[i].printAresta(im, minZ, d);
    }
}

void Face::rotateFace(char eixo, double cosAng, double sinAng){
    for(int i = 0; i < arestas.size() ; i++){
        arestas[i].rotateAresta(eixo, cosAng, sinAng);
    }
}

void Face::outFace(){
    for(int i = 0; i < arestas.size();i++){
        cout << "Aresta " << i << endl;
        arestas[i].outAresta();
    }
}
void Face::addAresta(Aresta aresta){
    arestas.push_back(aresta);
}

void Face::calcula_vetZ()
{
    arestas[0].calcula_vetDir();
    arestas[1].calcula_vetDir();
    Ponto vet1 = arestas[0].getvetDir();
    Ponto vet2 = arestas[1].getvetDir();
    this->vetZ = (vet1.getX() * vet2.getY()) - (vet1.getY() * vet2.getX());
}
float Face::getvetZ()
{
    return this->vetZ;
}
