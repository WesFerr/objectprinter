#include <iostream>
#include "../Headers/Ponto.h"

using namespace std;

Ponto::Ponto(){}
Ponto::~Ponto(){}

Ponto::Ponto(float X, float Y,float Z){
    x = X;
    y = Y;
    z = Z;
}

void Ponto::rotate(char eixo, double cosAng, double sinAng){
    switch(eixo){
        case 'x':
            rotateX(cosAng,sinAng);
            break;
        case 'y':
            rotateY(cosAng, sinAng);
            break;
        case 'z':
            rotateZ(cosAng, sinAng);
            break;
    
    }
}

void Ponto::rotateX(double cosAng, double sinAng){
    float ny,nz;
    ny = y*cosAng - z*sinAng;
    nz = y*sinAng + z*cosAng;  
    y = ny;
    z = nz;
}

void Ponto::rotateY(double cosAng, double sinAng){
    float nx,nz;
    nx = x*cosAng - z*sinAng;
    nz = x*sinAng + z*cosAng;     
    x = nx;
    z = nz;
}
void Ponto::rotateZ(double cosAng, double sinAng){
    float nx,ny;
    nx = x*cosAng - y*sinAng;
    ny = x*sinAng + y*cosAng;     
    x = nx;
    y = ny;
}

float* Ponto::calcProj(Ponto centro, Ponto p1, int s, float minZ, float d, float* proj){
    
    proj[0] = centro.getX() + (s * (p1.getX()/(p1.getZ()+(minZ+d))));
    proj[1] = centro.getY() + (s * (p1.getY()/(p1.getZ()+(minZ+d))));
    
    
    // Centro: centro da tela;
    // y vem do 3d
    // x vem do 3d
    // minZ: z minimo da figura
    // d: distancia focal
    // S: tamanho da vaca na tela em x
    
    return proj;
}
float Ponto::getX(){
    return x;
}

float Ponto::getY(){
    return y;
}

float Ponto::getZ(){
    return z;
}

void Ponto::setX(float x)
{
    this->x = x;
}
void Ponto::setY(float y)
{
    this->y = y;
}
void Ponto::setZ(float z)
{
    this->z = z;
}


