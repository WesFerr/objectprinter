#include <cstdlib>
#include <iostream>
#include "../Headers/Aresta.h"
#include "../Headers/Ponto.h"
#include "../Headers/image.hpp"

using namespace std;

Aresta::Aresta(){}
Aresta::~Aresta(){}

Aresta::Aresta(Ponto p1, Ponto p2){
    this->p1 = p1;
    this->p2 = p2;
    this->calcula_vetDir ();
}

void Aresta::outAresta(){
    cout << p1.getX() << " "<< p1.getY() << " "<< p1.getZ() << endl;
    cout << p2.getX() << " "<< p2.getY() << " "<< p2.getZ() << endl;
    cout << endl;
}

void Aresta::rotateAresta(char eixo, double cosAng, double sinAng){
    p1.rotate(eixo, cosAng, sinAng);
    p2.rotate(eixo, cosAng, sinAng);
}

void Aresta::printAresta(image *im,float minZ, float d){

    float *p1_2, *p2_2;
    Ponto centro = Ponto (im->getWidth()/2,im->getHeight()/2,0);
    p1_2 = (float*) malloc(2*sizeof(int));
    p2_2 = (float*) malloc(2*sizeof(int));
    
    Ponto::calcProj(centro, this->p1, im->getWidth()/2, minZ, d, p1_2);
    Ponto::calcProj(centro, this->p2, im->getWidth()/2, minZ, d, p2_2);
    
    im->drawline (p1_2[0], p1_2[1], p2_2[0], p2_2[1], 0, 0, 0);
    
    free(p1_2);
    free(p2_2);
    
}
void Aresta::calcula_vetDir ()
{
    this->vetDir.setX(p2.getX() - p1.getX());
    this->vetDir.setY(p2.getY() - p1.getY());
    this->vetDir.setZ(p2.getZ() - p1.getZ());
}

Ponto Aresta::getP1(){
    return this->p1;
}
Ponto Aresta::getP2(){
    return this->p2;
}
Ponto Aresta::getvetDir()
{
    return this->vetDir;
}
