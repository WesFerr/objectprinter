#include <iostream>
#include <fstream>
//#include <cstdlib>
#include "../Headers/Object.h"
#include "../Headers/Ponto.h"
#include "../Headers/Aresta.h"
#include "../Headers/image.hpp"

using namespace std;

int main(int argc, char** argv){

    if(argc < 7){
        cout << "argumentos insuficientes" << endl;
        return 1;
    }

    Object* bola = new Object;
    bola->readObject(argv[1]);
    Ponto centro (0, 0, 0);

    //image *fig1 = new image(300,300, *pix1, 255);

    //bola->outObject();
    /*
    Ponto p1 = Ponto(1,2,3);
    Ponto p2 = Ponto(2,3,4);
    Aresta a1 = Aresta(p1,p2);
    */
    pixel* pix1 = new pixel();
    pix1->r = 255;
    pix1->g = 255;
    pix1->b = 255;
    
    
    image* fig1 = new image(atoi(argv[3]), atoi(argv[4]), *pix1,255);
    bola->rotateObject('y',atoi(argv[6]));
    bola->printObject (fig1, atoi(argv[5]));
    /*
    Parametro d minima:
        Bola: 1
        Vaca: 5
        Elefante: 15
    
    
    */
    
    
    //bola->outObject();
    fig1->saveimage(argv[2]);
    
    delete pix1;
    delete fig1;
    return 0;
}
