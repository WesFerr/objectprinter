#ifndef OBJECT_H
#define OBJECT_H

#include <vector>
#include "../Headers/Face.h"
#include "image.hpp"

class Object{
    private:
        std::vector<Face> faces;
        int n_faces;
        float minZ; 
    public:
        Object();
        ~Object();
        std::vector<Face> getFaces();
        void addFace(Face face);
        void outObject();
        void printObject(image *im, float d);
        int getNFaces();
        void readObject(char* nomeArquivo);
        void rotateObject(char eixo, double angle);
        void sortFaces();
};

#endif
