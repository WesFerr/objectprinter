#ifndef FACE_H
#define FACE_H

#include <vector>
#include "Aresta.h"
#include "Ponto.h"
#include "image.hpp"

class Face{
    private:
        std::vector<Aresta> arestas;
        int n_arestas;
	    float vetZ;
	    float pZ;

    public:
        Face();
        ~Face();
        std::vector<Aresta> getArestas();
        void outFace();
        void addAresta(Aresta aresta);
        void printFace(image *im, float minZ, float d);
        void rotateFace(char eixo, double cosAng, double sinAng);
	    void calcula_vetZ();
        float getvetZ();
        void calcpZ();
        float getpZ();
        
};

#endif
