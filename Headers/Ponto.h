#ifndef PONTO_H
#define PONTO_H

class Ponto{
    private:
        float x;
        float y;
        float z;
    public:
        Ponto();
        ~Ponto();
        static float* calcProj(Ponto centro, Ponto p1, int s, float minZ, float d, float * proj);
        Ponto(float x, float y, float z);
        void rotate(char eixo, double cosAng, double sinAng);
        void rotateX(double cosAng, double sinAng);
        void rotateY(double cosAng, double sinAng);
        void rotateZ(double cosAng, double sinAng);
        float getX();
        float getY();
        float getZ();
	    void setX(float x);
	    void setY(float y);
	    void setZ(float z);

};

#endif
