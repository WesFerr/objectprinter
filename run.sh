#!/bin/bash 
  mkdir ppm
  echo compilando 
  g++ -o objectPrinter -std=c++11 ./Source/main.cpp ./Source/Object.cpp ./Source/Face.cpp ./Source/Aresta.cpp ./Source/Ponto.cpp ./Source/image.cpp -g
 
  echo executando ball  
  ./objectPrinter ./objetos/ball.obj ./ppm/ball.ppm 1000 1000 1 0
 
  echo executando cow 
  ./objectPrinter ./objetos/cow.obj ./ppm/cow.ppm 1000 1000 7 0
 
  echo executando elephant 
  ./objectPrinter ./objetos/elephant.obj ./ppm/elephant.ppm 1000 1000 15 -90
 
  echo terminou 
  echo  
  echo ./objetos 
  ls ./ppm 
