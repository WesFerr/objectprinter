#!/bin/bash
for filename in ./*.ppm; do
    convert $filename $filename.jpg
done
rm *.ppm
ffmpeg -framerate 24 -i cow%d.ppm.jpg output.mp4
rm *.jpg
